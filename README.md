# CEG Delivery System

An API for registering the delivery system of CEG.

## Installation

```sh
./scripts/start_develop.sh build
```

or manually:
```sh
docker-compose -f docker/docker-compose.yml build
```

## Run develop mode

> This mode linked the code in a volume.
```sh
./scripts/start_develop.sh
```

For running the migrations before run use the flag `makemigrations` as:
```sh
./scripts/start_develop.sh makemigrations
```

## Stop running application

```sh
./scripts/stop_develop.sh
```


### Who do I talk to?

* [Martin Reyes](martingreyes27@gmail.com)
