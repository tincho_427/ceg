#!/bin/bash

function wait_for_db {
    echo "Waiting db startup..."
    until docker-compose -f docker/docker-compose.yml exec db pg_isready ; do sleep 1 ; done
    echo "DB is ready!"
}

# If called with 'build', build the project first
if [[ "$1" == build ]]
then
    docker-compose -f docker/docker-compose.yml build
fi

# Start the project
docker-compose -f docker/docker-compose.yml up -d

if [[ "$1" == makemigrations ]]
then
    # Wait for postgres
    wait_for_db

    echo "Making new migrations"
    docker-compose -f docker/docker-compose.yml exec ceg_api python3 manage.py makemigrations
    echo "Done!"
fi

# Wait for postgres
echo "Running migrations"
wait_for_db
docker exec -it ceg_api python3 manage.py migrate
echo "Migrations done!"

# Attaching
docker exec -it ceg_api python3 manage.py runserver 0.0.0.0:8000
